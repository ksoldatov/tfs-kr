package com.ksoldatov.kr.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.ksoldatov.kr.R;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    public static String MAP_EXTRA = "MAP_EXTRA";
    public static String GEO_LAT = "GEO_LAT";
    public static String GEO_LONG = "GEO_LONG";

    private boolean refreshFragmant(int itemId, Bundle args) {
        Fragment selectedFragment = null;
        String fragmentName = null;
        switch (itemId) {
            case R.id.action_history:
                selectedFragment = HistoryFragment.newInstance();
                break;
            case R.id.action_map:
                selectedFragment = CounterPartyMapFragment.newInstance();
                break;
            case R.id.action_search:
                selectedFragment = SearchFragment.newInstance();
                break;
        }
        FragmentManager manager = getSupportFragmentManager();
        if (selectedFragment != null) {
            fragmentName = selectedFragment.getClass().getName();
            selectedFragment.setArguments(args);
        }
        boolean fragmentPopped = manager.popBackStackImmediate(fragmentName, 0);
        if (!fragmentPopped) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_layout, selectedFragment);
            ft.addToBackStack(fragmentName);
            ft.commit();
        }
        return true;
    }

    private void initInflate() {
        Fragment selectedFragment = SearchFragment.newInstance();
        String fragmentName = selectedFragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.frame_layout, selectedFragment);
        ft.addToBackStack(fragmentName);
        ft.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return refreshFragmant(item.getItemId(), new Bundle());
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            int mapFragmentId;
            String geoLat, geoLong;
            mapFragmentId = intent.getIntExtra(MAP_EXTRA, 0);
            geoLat = intent.getStringExtra(GEO_LAT);
            geoLong = intent.getStringExtra(GEO_LONG);
            if (mapFragmentId != 0) {
                Bundle args = new Bundle();
                args.putString(GEO_LAT, geoLat);
                args.putString(GEO_LONG, geoLong);
                refreshFragmant(mapFragmentId, args);
            }
        }

        if (savedInstanceState == null && !intent.hasExtra(MAP_EXTRA)) {
            initInflate();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

}

